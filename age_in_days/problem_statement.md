## Goal

Given a person's `age` and a current `year`, calculate the total number of days that have passed since that person's birth (assume that their birthday is on January 1st) until the start of the current `year`.
(e.g. current year: 2022, calculate until 01-01-2022)
A leap year has 366 days.

---

## Input

Line 1: An integer `age` for a person's `age`.
Line 2: An integer `year` for the current year.

---

## Output

A single line containing an integer for the total number of days that someone has lived.

---

## Constraints

0 < `age` ≤ 118
1900 ≤ `year` ≤ 2100

---

## Example

#### Input

```
1
2021
```

#### Output

```
367
```
