const testCase1 = `1
2021`;

const [age, year] = testCase1.split(/\r|\r?\n/).map(Number);

console.log(
  (new Date(`${year}`).getTime() - new Date(`${year - age}`).getTime())
  / (1000 * 3600 * 24)
  + 1
);
