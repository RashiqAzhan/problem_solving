const testCase1 = `1
2021`;

const [age, year] = testCase1.split(/\r|\r?\n/).map(Number);

const isLeapYear = (year: number): boolean =>
  (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;

console.log(
  Array.from({length: age}, (_, i) => year - age + i)
       .map<number>((year) => isLeapYear(year) ? 366 : 365)
       .reduce((a, b) => a + b),
);
