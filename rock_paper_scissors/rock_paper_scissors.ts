const testCase1 = `ROCK SCISSORS`;

type Move = "ROCK" | "PAPER" | "SCISSORS";

const player1Matrix = {
  "ROCK": {
    "ROCK": "DRAW",
    "PAPER": "PLAYER2",
    "SCISSORS": "PLAYER1",
  },
  "PAPER": {
    "ROCK": "PLAYER1",
    "PAPER": "DRAW",
    "SCISSORS": "PLAYER2",
  },
  "SCISSORS": {
    "ROCK": "PLAYER2",
    "PAPER": "PLAYER1",
    "SCISSORS": "DRAW",
  },
};

const [player1Move, player2Move] = testCase1.split(" ").map(move => move as Move);

console.log(player1Matrix[player1Move][player2Move]);
