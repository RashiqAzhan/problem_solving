const testCase1 = 1;
const testCase1Expected = `.*.
.*.`;

const testCase2 = 3;
const testCase2Expected = `...*...
..***..
.*****.
...*...`;

const testCase3 = 4;
const testCase3Expected = `....*....
...***...
..*****..
.*******.
....*....`;

const testCase4 = 5;
const testCase4Expected = `.....*.....
....***....
...*****...
..*******..
.*********.
.....*.....`;

const testCase5 = 12;
const testCase5Expected = `............*............
...........***...........
..........*****..........
.........*******.........
........*********........
.......***********.......
......*************......
.....***************.....
....*****************....
...*******************...
..*********************..
.***********************.
............*............`;

const width = testCase1 * 2 + 1;

console.log(
  Array.from({length: testCase1}, (_, index) => "*".repeat(index * 2 + 1))
       .concat("*")
       .map((branch) => {
         const air = ".".repeat((width - branch.length) / 2);
         return `${air}${branch}${air}`;
       })
       .join("\n"),
);
