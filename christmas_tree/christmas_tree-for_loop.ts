const testCase1 = 1;
const testCase1Expected = `.*.
.*.`;

const testCase2 = 3;
const testCase2Expected = `...*...
..***..
.*****.
...*...`;

const testCase3 = 4;
const testCase3Expected = `....*....
...***...
..*****..
.*******.
....*....`;

const testCase4 = 5;
const testCase4Expected = `.....*.....
....***....
...*****...
..*******..
.*********.
.....*.....`;

const testCase5 = 12;
const testCase5Expected = `............*............
...........***...........
..........*****..........
.........*******.........
........*********........
.......***********.......
......*************......
.....***************.....
....*****************....
...*******************...
..*********************..
.***********************.
............*............`;

const branchBuilder = (level: number, branchWidth: number): string => {
  const halfWidth = Math.floor(branchWidth / 2);
  const air = ".".repeat(halfWidth - level);
  return `${air}${"*".repeat(level * 2 + 1)}${air}`;
};


const treeWidth = testCase1 * 2 + 1;

for (let i = 0; i < testCase1; ++i) {
  console.log(branchBuilder(i, treeWidth));
}

console.log(
  ".".repeat(treeWidth / 2)
  + `*` +
  ".".repeat(treeWidth / 2),
);
