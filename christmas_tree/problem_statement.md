## Goal

It's Christmas !
Display a pretty tree depending on its height `N` given on standard input.
The tree is made of `N + 1`: `N` for its branches and a one for its trunk

To get your tree correctly centered, each stage of the tree must be composed of an odd number of stars.
Fill in the empty space with points.


---

## Input

Line 1: an integer `N` (number of stages of the tree).

---

## Output

`N` lines: the tree branches (as many lines as the tree height).
Last line: the tree trunk.

These lines must only be made of points and stars.

---

## Constraints

1 ≤ `N` ≤ 20

---

## Example

#### Input

```
4
```

#### Output

```
....*....
...***...
..*****..
.*******.
....*....
```
