## Goal

Your goal is to print a specified term of a series of integers,
which is contructed using the following rules :

`S`[2k+1] = `S`[2k  ] * `MUL` + `INC`
`S`[2k+2] = `S`[2k+1] * `MUL` - `INC`
for any integer `k` >= 0

Example :
Given `S0` = 1, `MUL` = 2, `INC` = 1,
the series will be : 1 3 5 11 21 43 85 ...

---

## Input

Line 1 : An integer `S0` as the series's first number (`S`[0]).
Line 2 : An integer `N` as the index (0-based) of the term to print.
Line 3 : 2 integers `MUL` and `INC` separated by a space.

---

## Output

The `N`th term of the series (as described in the statement).

---

## Constraints

0 <= `N` <= 8
-10 <= S0, `MUL`, `INC` <= 10

---

## Example

#### Input

```
1
1
2 1
```

#### Output

```
3
```
