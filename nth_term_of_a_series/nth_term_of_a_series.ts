const testCase1 = `1
1
2 1`;

const [firstTerm, nthTerm, [multiplier, increment]] =
  testCase1
    .split(/\r|\r?\n/)
    .map((line) =>
      line.length === 1
      ? Number(line)
      : line.split(" ")
            .map(Number),
    ) as [number, number, [number, number]];

console.log(
  Array.from({length: nthTerm}, (_, i) => i)
       .reduce((nthTermValue, index) =>
           nthTermValue
           * multiplier
           + (index % 2 === 0 ? increment : -increment)
         , firstTerm),
);
