def remove_duplicates(array: list[int]):
    starting_pointer = 0
    ending_pointer = 1

    while ending_pointer < len(array):
        if array[starting_pointer] == array[ending_pointer]:
            array.pop(ending_pointer)
        else:
            starting_pointer += 1
            ending_pointer += 1

    return len(array)


def main():
    array = [2, 2, 2, 11]
    result = remove_duplicates(array)
    print(result)


main()
