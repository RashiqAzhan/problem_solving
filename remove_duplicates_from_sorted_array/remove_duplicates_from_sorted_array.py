def remove_duplicates(array: list[int]):
    duplicate_index = 0

    for i in range(len(array)):
        if array[duplicate_index] != array[i]:
            duplicate_index += 1
            array[duplicate_index] = array[i]

    return duplicate_index + 1


def main():
    array = [2, 3, 3, 3, 6, 9, 9]
    result = remove_duplicates(array)
    print(result)


main()
