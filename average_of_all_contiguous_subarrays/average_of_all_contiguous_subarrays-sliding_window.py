def avg_of_contiguous_subarray(array: list[int], subarray_length: int):
    result = []

    first_subarray_sum = sum(array[:subarray_length])
    subarray_avg = first_subarray_sum / subarray_length
    result.append(subarray_avg)
    previous_subarray_sum = first_subarray_sum

    for i in range(subarray_length, len(array)):
        next_subarray_sum = previous_subarray_sum - \
            array[i - subarray_length] + array[i]
        subarray_avg = next_subarray_sum / subarray_length
        result.append(subarray_avg)
        previous_subarray_sum = next_subarray_sum

    return result


def main():
    array = [1, 3, 2, 6, -1, 4, 1, 8, 2]
    k = 5
    result = avg_of_contiguous_subarray(array, k)
    print(result)


main()
