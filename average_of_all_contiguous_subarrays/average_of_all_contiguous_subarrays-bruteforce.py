def average(array: list[int]):
    return sum(array) / len(array)


def avg_of_contiguous_subarray(array: list[int], subarray_length: int):
    result = []
    for i in range(len(array)):
        if i + subarray_length <= len(array):
            result.append(average(array[i:i + subarray_length]))
    return result


def main():
    array = [1, 3, 2, 6, -1, 4, 1, 8, 2]
    k = 5
    result = avg_of_contiguous_subarray(array, k)
    print(result)


main()
