def no_repeat_substring(string: str):
    character_occurrences = {}
    max_substring_len = 0
    window_start = 0

    for window_end, character in enumerate(string):
        if character not in character_occurrences:
            character_occurrences[character] = 0
        character_occurrences[character] += 1

        while character_occurrences[character] > 1:
            window_starting_character = string[window_start]
            character_occurrences[window_starting_character] -= 1
            window_start += 1

        max_substring_len = max(
            max_substring_len,
            window_end - window_start + 1)

    return max_substring_len


def main():
    string = "abccde"
    result = no_repeat_substring(string)
    print(result)


main()
