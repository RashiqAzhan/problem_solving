def no_repeat_substring(string: str):
    character_occurrences = {}
    max_substring_len = 0
    window_start = 0

    for window_end, character in enumerate(string):
        if character not in character_occurrences:
            max_substring_len = max(
                max_substring_len,
                window_end - window_start + 1)
        else:
            window_start = max(
                window_start,
                character_occurrences[character] + 1)
        character_occurrences[character] = window_end

    return max_substring_len


def main():
    string = "abbbbaiopkbvdddddhhhhhhhhiopjkl"
    result = no_repeat_substring(string)
    print(result)


main()
