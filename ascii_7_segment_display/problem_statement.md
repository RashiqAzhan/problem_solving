## Goal

You wake up after a long nap, and glance at your alarm clock. The alarm clock displays four digits, using 7 segments for each digit.

What time is it?

Digits look like this:

```
 _     _  _     _  _  _  _  _ 
| |  | _| _||_||_ |_   ||_||_|
|_|  ||_  _|  | _||_|  ||_| _|
```

---

## Input

3 lines of width 13: The alarm clock display in ASCII art, 7 segments per digit, using `|` and `_` for luminous segments and ` `(space) for dark segments, with hours separated from minutes by two dots `.` in a column.

---

## Output

1 line: A string corresponding to the alarm clock display in `hh`:`mm` format.

---

## Constraints

Time `hh`:`mm` satisfies 00≤`hh`≤23, 00≤`mm`≤59.

---

## Example

#### Input

```
    _   _    
  | _|. _||_|
  ||_ . _|  |
```

#### Output

```
12:34
```
