const testCase1 = `    _   _    
  | _|. _||_|
  ||_ . _|  |`;

const testCase2 = ` _  _   _  _ 
| |  |.|_ |_ 
|_|  |. _||_|`;

const testCase3 = ` _  _   _  _ 
| ||_|.| ||_|
|_||_|.|_| _|`;

const testCase4 = `             
  |  |.  |  |
  |  |.  |  |`;

const testCase5 = ` _  _   _  _ 
 _| _|. _| _|
|_ |_ .|_ |_ `;

const testCase6 = ` _  _   _  _ 
| | _|. _| _|
|_| _|. _| _|`;

const testCase7 = `             
  ||_|.|_||_|
  |  |.  |  |`;

const testCase8 = `    _   _  _ 
  ||_ .|_ |_ 
  | _|. _| _|`;

const testCase9 = ` _  _      _ 
| ||_ .|_||_ 
|_||_|.  ||_|`;

const testCase10 = ` _  _   _  _ 
| |  |. _|  |
|_|  |.|_   |`;

const testCase11 = `    _   _  _ 
  ||_|. _||_|
  ||_|. _||_|`;

const testCase12 = `    _   _  _ 
  ||_|.|_ |_|
  | _|. _| _|`;

const testCase13 = ` _  _   _  _ 
| || |.| || |
|_||_|.|_||_|`;

const asciiDigits = ` _     _  _     _  _  _  _  _ 
| |  | _| _||_||_ |_   ||_||_|
|_|  ||_  _|  | _||_|  ||_| _|`;

const segmentDigits: {[key: string]: number} =
  asciiDigits
    .split(/\r|\r?\n/)
    .reduce((digits: string[], line) =>
        digits.map((digit, index) => digit.concat(line.slice(index * 3, index * 3 + 3)))
      , Array(10).fill(""))
    .reduce((segmentDigit, digit, index) =>
        ({...segmentDigit, [digit]: index})
      , {},
    );

const clockDigits = testCase1
  .split(/\r|\r?\n/)
  .map((line) => line.slice(0, 6) + line.slice(7))
  .reduce((digits, line) =>
      digits.map((digit, index) => digit.concat(line.slice(index * 3, index * 3 + 3)))
    , Array(4).fill(""));

const time =
  clockDigits.reduce((time, digit, index) => {
    if (index === 2) {
      time += ":";
    }

    return time.concat(segmentDigits[digit]);
  }, "");

console.log(time);
