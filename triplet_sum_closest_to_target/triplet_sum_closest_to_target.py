def find_triplet(array: list[int], target_sum: int):
    array = sorted(array)
    smallest_triplet_sum = float("inf")
    smallest_triplet_sum_difference = float("inf")
    for i in range(len(array) - 2):
        if i > 0 and array[i] == array[i - 1]:
            continue
        target = - array[i] + target_sum
        pair = find_sum_pair(
            array,
            target,
            left_pointer=i + 1,
            right_pointer=len(array) - 1)
        current_triplet_sum = pair + array[i]
        current_triplet_sum_difference = abs(current_triplet_sum - target_sum)
        if current_triplet_sum_difference < smallest_triplet_sum_difference:
            smallest_triplet_sum = current_triplet_sum
            smallest_triplet_sum_difference = current_triplet_sum_difference

    return smallest_triplet_sum


def find_sum_pair(
        array: list[int],
        target: int,
        left_pointer: int,
        right_pointer: int):
    smallest_difference = float("inf")
    smallest_pair_sum = 0

    while left_pointer < right_pointer:
        left_pointer_value = array[left_pointer]
        right_pointer_value = array[right_pointer]
        pointers_sum = left_pointer_value + right_pointer_value
        current_difference = abs(pointers_sum - target)
        if current_difference < smallest_difference:
            smallest_difference = current_difference
            smallest_pair_sum = pointers_sum
        if current_difference < target:
            left_pointer += 1
        else:
            right_pointer -= 1

    return smallest_pair_sum


def main():
    array = [1, 0, 1, 1]
    target_sum = 100
    result = find_triplet(array, target_sum)
    print(result)


main()
