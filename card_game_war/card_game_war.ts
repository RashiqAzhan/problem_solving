const testCase1 = `Ace
2`;

const CardValues = Object.freeze(
  {
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "10": 10,
    "Jack": 11,
    "Queen": 12,
    "King": 13,
    "Ace": 14,
  });

type Card = keyof typeof CardValues;

const Card = (arg: string): Card => arg as Card;
const playBattle = (card1: Card, card2: Card): {
  winner: "Player1" | "Player2",
  quality: "Really Good" | "Good" | "Bad" | "Really Bad" | "Unknown"
} | "war" | "unknown" => {
  const distance = Math.abs(CardValues[card1] - CardValues[card2]);
  const winner = CardValues[card1] > CardValues[card2] ? "Player1" : "Player2";

  switch (true) {
    case distance === 0:
      return "war";
    case new Set([1, 2, 3]).has(distance):
      return {winner, quality: "Really Good"};
    case new Set([4, 5, 6]).has(distance):
      return {winner, quality: "Good"};
    case new Set([7, 8, 9]).has(distance):
      return {winner, quality: "Bad"};
    case new Set([10, 11, 12]).has(distance):
      return {winner, quality: "Really Bad"};
    default:
      return "unknown";
  }
};

const [player1Card, player2Card]: Card[] = testCase1.split(/\r|\r?\n/).map(Card);

Object.values(playBattle(player1Card, player2Card))
      .forEach((value) => console.log(value));
