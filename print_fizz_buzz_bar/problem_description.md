## Goal

Print numbers from 1 to N each on a new line except:

If the number is divisble by 3 print Fizz
If the number is divisible by 5, print Buzz
If the number is divisible by 4, print Bar
If the number is divisible by 3 and 4, but not 5, print FizzBar
If the number is divisible by 5 and 4, but not 3, print BuzzBar
If the number is divisible by 5 and 3, but not 4, print FizzBuzz
If the number is divisible by all three, print FizzBuzzBar

---

## Input

A number N, which the range goes up to (inclusive of N).

---

## Output

Numbers, each on a new line. Any number divided by 3, 5, or 4 is replaced with its complimentary word(s).

---

## Constraints

N>0

---

## Example

#### Input

```
5
```

#### Output

```
1
2
Fizz
Bar
Buzz
```
