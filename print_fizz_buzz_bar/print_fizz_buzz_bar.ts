const testCase1 = 5;
const testCase1Expected = `1
2
Fizz
Bar
Buzz`;

const testCase2 = 75;
const testCase2Expected = `1
2
Fizz
Bar
Buzz
Fizz
7
Bar
Fizz
Buzz
11
FizzBar
13
14
FizzBuzz
Bar
17
Fizz
19
BuzzBar
Fizz
22
23
FizzBar
Buzz
26
Fizz
Bar
29
FizzBuzz
31
Bar
Fizz
34
Buzz
FizzBar
37
38
Fizz
BuzzBar
41
Fizz
43
Bar
FizzBuzz
46
47
FizzBar
49
Buzz
Fizz
Bar
53
Fizz
Buzz
Bar
Fizz
58
59
FizzBuzzBar
61
62
Fizz
Bar
Buzz
Fizz
67
Bar
Fizz
Buzz
71
FizzBar
73
74
FizzBuzz`;

const testCase3 = 20;
const testCase3Expected = `1
2
Fizz
Bar
Buzz
Fizz
7
Bar
Fizz
Buzz
11
FizzBar
13
14
FizzBuzz
Bar
17
Fizz
19
BuzzBar`;

const testCase4 = 62;
const testCase4Expected = `1
2
Fizz
Bar
Buzz
Fizz
7
Bar
Fizz
Buzz
11
FizzBar
13
14
FizzBuzz
Bar
17
Fizz
19
BuzzBar
Fizz
22
23
FizzBar
Buzz
26
Fizz
Bar
29
FizzBuzz
31
Bar
Fizz
34
Buzz
FizzBar
37
38
Fizz
BuzzBar
41
Fizz
43
Bar
FizzBuzz
46
47
FizzBar
49
Buzz
Fizz
Bar
53
Fizz
Buzz
Bar
Fizz
58
59
FizzBuzzBar
61
62`;

console.log(Array.from({length: testCase1}, (_, i) => ++i)
    .reduce((output, number) => {
        let ans = "";

        if (number % 3 === 0) {
            ans += "Fizz";
        }
        if (number % 5 === 0) {
            ans += "Buzz";
        }
        if (number % 4 === 0) {
            ans += "Bar";
        }

        return ans === "" ? [...output, number.toString()] : [...output, ans];
    }, [] as string[])
    .join("\n"));
