def square_array(array: list[int]):
    squared_array = [0 for _ in range(len(array))]
    squared_array_insertion_index = len(squared_array) - 1
    left_pointer = 0
    right_pointer = len(array) - 1

    while left_pointer <= right_pointer:
        left_element_squared = array[left_pointer] * array[left_pointer]
        right_element_squared = array[right_pointer] * array[right_pointer]
        if left_element_squared < right_element_squared:
            squared_array[squared_array_insertion_index] = right_element_squared
            right_pointer -= 1
        else:
            squared_array[squared_array_insertion_index] = left_element_squared
            left_pointer += 1
        squared_array_insertion_index -= 1

    return squared_array


def is_negative(value: int):
    if value < 0:
        return True
    else:
        return False


def main():
    array = [-3, -1, 0, 1, 2]
    result = square_array(array)
    print(result)


main()
