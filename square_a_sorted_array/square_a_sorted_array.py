def square_array(array: list[int]):
    return sorted([x * x for x in array])


def main():
    array = [-2, -1, 0, 2, 3]
    result = square_array(array)
    print(result)


main()
