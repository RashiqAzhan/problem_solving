def fruits_into_baskets(fruits: list[str]):
    baskets = {}
    max_fruits = 0
    window_start = 0
    for window_end, fruit in enumerate(fruits):
        if fruit not in baskets:
            baskets[fruit] = 0
        baskets[fruit] += 1

        while len(baskets) > 2:
            first_fruit_in_window = fruits[window_start]
            baskets[first_fruit_in_window] -= 1
            if baskets[first_fruit_in_window] == 0:
                del baskets[first_fruit_in_window]
            window_start += 1

        max_fruits = max(max_fruits, window_end - window_start + 1)

    return max_fruits


def main():
    fruits = ['A', 'B', 'C', 'A', 'C']
    result = fruits_into_baskets(fruits)
    print(result)


main()
