def find_triplets_with_zero_sum(array: list[int]):
    triplets = []
    array = sorted(array)

    for i in range(len(array) - 1):
        if i > 0 and array[i - 1] == array[i]:
            continue
        target = -array[i]

        if pairs := find_sum_pairs(
                array,
                target,
                left_pointer=i + 1,
                right_pointer=len(array) - 1):
            for pair in pairs:
                triplet = [array[i], *pair]
                triplets.append(triplet)
    return triplets


def find_sum_pairs(
        array: list[int],
        target: int,
        left_pointer: int,
        right_pointer: int):
    pairs = []

    while left_pointer < right_pointer:
        left_pointer_value = array[left_pointer]
        right_pointer_value = array[right_pointer]
        current_sum = left_pointer_value + right_pointer_value
        if current_sum < target:
            left_pointer += 1
        elif current_sum > target:
            right_pointer -= 1
        else:
            pair = [left_pointer_value, right_pointer_value]
            pairs.append(pair)

            left_pointer += 1
            while left_pointer < right_pointer and array[left_pointer - 1] == array[left_pointer]:
                left_pointer += 1

            right_pointer -= 1
            while left_pointer < right_pointer and array[right_pointer] == array[right_pointer + 1]:
                right_pointer -= 1

    return pairs


def main():
    array = [-3, 0, 1, 2, -1, 1, -2, 1, 2, -10, 13, -1]
    result = find_triplets_with_zero_sum(array)
    print(result)


main()
