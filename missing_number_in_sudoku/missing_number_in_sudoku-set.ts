const testCase1 = `8372?9514`;

const gridSet = new Set(
  testCase1
    .replace("?", "")
    .split("")
    .map(Number),
);

console.log(
  Array.from({length: 9}, (_, i) => ++i)
       .filter((number) => !gridSet.has(number))
       .at(0),
);
