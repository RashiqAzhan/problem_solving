const testCase1 = `8372?9514`;

const gridSum = Array.from({length: 10}, (_, i) => i).reduce((a, b) => a + b);

console.log(
  gridSum
  - [...testCase1]
    .map(Number)
    .filter((char) => !Number.isNaN(char))
    .reduce((a, b) => a + b),
);
