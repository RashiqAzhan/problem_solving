const testCase1 = `5 -2
3
3
8
-1`;
const testCase1Expected = `13
38
-7`

const testCase2 = `1 0
3
1
2
3`;
const testCase2Expected = `1
2
3`

const testCase3 = `0 5
3
1
45
3`;
const testCase3Expected = `5
5
5`

const testCase4 = `2 3
5
0
2
4
6
1`;
const testCase4Expected = `3
7
11
15
5`

const testCase5 = `-4 4
5
1
3
5
7
2`;
const testCase5Expected = `0
-8
-16
-24
-4`

const linearFunction = (a: number, b: number, x: number): number => a * x + b;

const parseData = (data: string): [number, number, number[]] => {
    const parsedData = data.split(/\r?\n|\r/);

    const [a, b] = parsedData.at(0)!.split(" ").map(Number);
    const xValues = parsedData.slice(2).map(Number);

    return [a, b, xValues];
};

const main = () => {
    const [a, b, xValues] = parseData(testCase1);

    const yValues = xValues.map(x => linearFunction(a, b, x));

    yValues.forEach(y => console.log(y));
};

main();
