def insert_interval(intervals, new_interval):
    merged = []
    start = 0
    end = 1

    merged_start = 0
    merged_end = 0
    last_index = 0
    for i, interval in enumerate(intervals):
        if interval[end] < new_interval[start]:
            merged.append(interval)
            continue
        elif interval[start] > new_interval[end]:
            last_index = i
            break
        else:
            merged_start = min(new_interval[start], interval[start])
            merged_end = max(new_interval[end], interval[end])

    merged.append([merged_start, merged_end])
    if last_index > 0:
        merged += intervals[last_index:]

    return merged


def main():
    print(f"Intervals after inserting the new interval: {str(insert_interval([[1, 3], [5, 7], [8, 12]], [4, 6]))}")
    print(f"Intervals after inserting the new interval: {str(insert_interval([[1, 3], [5, 7], [8, 12]], [4, 10]))}")
    print(f"Intervals after inserting the new interval: {str(insert_interval([[2, 3], [5, 7]], [1, 4]))}")


main()
