const testCase1 = `5
1 2 3 4 5`;

const testCase2 = `10
161 594 683 -63 541 -380 376 877 705 597`;

const [_, numbers] = testCase1.split(/\r|\r?\n/);

numbers.split(" ").map(Number).forEach((number) => {
  console.log(`${number % 2 === 0 ? "[ ]" : "[x]"} ${number}`);
});
