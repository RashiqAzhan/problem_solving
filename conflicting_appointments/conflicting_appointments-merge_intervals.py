def can_attend_all_appointments(intervals: list[list[int, int]]) -> bool:
    sorted_intervals = sorted(intervals, key=lambda x: x[1])

    for i in range(1, len(intervals)):
        first_interval_end = sorted_intervals[i - 1][1]
        second_interval_start = sorted_intervals[i][0]

        if second_interval_start < first_interval_end:
            return False

    return True


def main():
    print(f"Can attend all appointments: {str(can_attend_all_appointments([[1, 4], [2, 5], [7, 9]]))}")
    print(f"Can attend all appointments: {str(can_attend_all_appointments([[6, 7], [2, 4], [8, 12]]))}")
    print(f"Can attend all appointments: {str(can_attend_all_appointments([[4, 5], [2, 3], [3, 6]]))}")


main()
