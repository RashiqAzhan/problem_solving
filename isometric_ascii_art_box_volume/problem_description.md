## Goal

For the summer, I need to fill my swimming pool with water.

I have drawn a schema of the pool using characters `-`, `\`, `|` and some others for better looking.

Each character represents 1 meter, except dash `-` which appears to be smaller and represents half a meter.

---

## Input

*Line 1*: An integer n for the number of lines describing the pool

*Next n lines*: A string representing isometric drawing of the pool, using characters `-`, `\ ` and `|`.

---

## Output

An integer, the volume of the pool in cubic meters.

---

## Constraints

4 ≤ n ≤ 30

---

## Example

#### Input

```
4
.--.  
|\  \ 
 \|--|
  `--'
```

#### Output

```
1
```
