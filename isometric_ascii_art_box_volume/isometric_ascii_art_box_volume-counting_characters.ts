const testCase1 = `4
.--.
|\\  \\
 \\|--|
  \`--'`;
console.log(testCase1);

const calculateIsoBoxVol = (art: string): number => {
    const [boxLength, boxWidth, boxHeight] = art
        .split("")
        .reduce(([length, width, height], char) =>
                [
                    char === "-" ? ++length : length
                    , char === "\\" ? ++width : width
                    , char === "|" ? ++height : height,
                ]
            , [0, 0, 0])
        .map(side => side / 3)

    return 0.5 * boxLength * boxWidth * boxHeight;
}

console.log(calculateIsoBoxVol(testCase1));
