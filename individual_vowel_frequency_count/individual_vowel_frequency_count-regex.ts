const testCase1 = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`;
const testCase2 = `Uppercase and lowercase!`;
const testCase3 = `C0dInG4me`;
const testCase4 = `BlackAndYellow`;

const vowelFrequency = [...testCase1]
  .map((char) => char.toLocaleLowerCase())
  .reduce((vowelFrequency, char) =>
      char.match(/[aeiou]/)
      ? {...vowelFrequency, [char]: ++vowelFrequency[char as keyof typeof vowelFrequency]}
      : vowelFrequency
    , {a: 0, e: 0, i: 0, o: 0, u: 0});

console.log(Object.values(vowelFrequency).join(" "));
