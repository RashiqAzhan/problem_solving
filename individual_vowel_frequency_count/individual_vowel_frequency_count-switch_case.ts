const testCase1 = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`;
const testCase2 = `Uppercase and lowercase!`;
const testCase3 = `C0dInG4me`;
const testCase4 = `BlackAndYellow`;

const ans =
  Object.values(
    [...testCase1]
      .map((char) => char.toLocaleLowerCase())
      .reduce(
        (vowelFrequency, char) => {
          switch (char) {
            case "a":
            case "e":
            case "i":
            case "o":
            case "u":
              vowelFrequency[char] += 1;
            default:
              console.log(vowelFrequency);
              return vowelFrequency;
          }
        }
        , {a: 0, e: 0, i: 0, o: 0, u: 0},
      ))
        .join(" ");

console.log(ans);
