class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next


def has_cycle(head: Node):
    first_pointer = head
    second_pointer = head
    while second_pointer.next.next is not None:
        if(second_pointer.value < first_pointer.value):
            return True

        first_pointer = first_pointer.next
        second_pointer = second_pointer.next.next

    return False


def main():
    head = Node(1)
    head.next = Node(2)
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)
    head.next.next.next.next.next = Node(6)
    print(f"LinkedList has cycle: {has_cycle(head)}")

    head.next.next.next.next.next.next = head.next.next
    print(f"LinkedList has cycle: {has_cycle(head)}")

    head.next.next.next.next.next.next = head.next.next.next
    print(f"LinkedList has cycle: {has_cycle(head)}")


main()
