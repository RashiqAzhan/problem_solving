def longest_substring_with_distinct_characters(
        string: str, distinct_characters: int):
    character_occurrence = {}
    max_substring_len = 0
    substring_starting_index = 0

    for substring_ending_index in range(len(string)):
        substring_ending_character = string[substring_ending_index]
        if substring_ending_character not in character_occurrence:
            character_occurrence[substring_ending_character] = 0
        character_occurrence[substring_ending_character] += 1

        while len(character_occurrence) > distinct_characters:
            substring_starting_character = string[substring_starting_index]
            character_occurrence[substring_starting_character] -= 1
            if character_occurrence[substring_starting_character] == 0:
                del character_occurrence[substring_starting_character]
            substring_starting_index += 1

        max_substring_len = max(
            max_substring_len,
            substring_ending_index -
            substring_starting_index +
            1)

    return max_substring_len


def main():
    string = "araacijjjjjj"
    distinct_characters = 2
    result = longest_substring_with_distinct_characters(
        string, distinct_characters)
    print(result)


main()
