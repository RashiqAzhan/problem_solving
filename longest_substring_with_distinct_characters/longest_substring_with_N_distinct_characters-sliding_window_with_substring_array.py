def longest_substring_with_distinct_characters(
        string: str, distinct_characters: int):
    substring = []
    character_occurrences = {}
    max_substring_len = 0

    i = 0
    while i < len(string):
        if string[i] not in character_occurrences and len(
                character_occurrences) < distinct_characters:
            character_occurrences[string[i]] = 1
            substring.append(string[i])
            i += 1
        elif string[i] in character_occurrences and len(character_occurrences) <= distinct_characters:
            character_occurrences[string[i]
                                  ] = character_occurrences[string[i]] + 1
            substring.append(string[i])
            i += 1
        else:
            popped_char = substring.pop(0)
            if character_occurrences[popped_char] == 1:
                del character_occurrences[popped_char]
            else:
                character_occurrences[popped_char] = character_occurrences[popped_char] - 1
        max_substring_len = max(max_substring_len, len(substring))

    return max_substring_len


def main():
    string = "araacijjjjj"
    distinct_characters = 2
    result = longest_substring_with_distinct_characters(
        string, distinct_characters)
    print(result)


main()
