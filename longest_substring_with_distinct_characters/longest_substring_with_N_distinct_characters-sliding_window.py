def longest_substring_with_distinct_characters(
        string: str, distinct_characters: int):
    substring_len = 0
    character_occurrence = {}
    max_substring_len = 0
    subarray_starting_index = 0

    i = 0
    while i < len(string):
        if string[i] not in character_occurrence and len(
                character_occurrence) < distinct_characters:
            character_occurrence[string[i]] = 1
            i += 1
            substring_len += 1
        elif string[i] in character_occurrence and len(character_occurrence) <= distinct_characters:
            character_occurrence[string[i]
                                 ] = character_occurrence[string[i]] + 1
            i += 1
            substring_len += 1
        else:
            popped_char = string[subarray_starting_index]
            subarray_starting_index += 1
            substring_len -= 1
            if character_occurrence[popped_char] == 1:
                del character_occurrence[popped_char]
            else:
                character_occurrence[popped_char] = character_occurrence[popped_char] - 1
        max_substring_len = max(max_substring_len, substring_len)

    return max_substring_len


def main():
    string = "araacijjj"
    distinct_characters = 2
    result = longest_substring_with_distinct_characters(
        string, distinct_characters)
    print(result)


main()
