const testCase1 = `Are you stupid?`;

console.log(
  testCase1
    .split(" ")
    .map((word) =>
      word
        .replace(/[.'!?]/g, "")
        .length % 2 === 0
      ? word.toUpperCase()
      : word)
    .join(" "),
);
