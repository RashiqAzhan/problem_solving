## Goal

You got a text message on the phone from your mom. She is always angry, and, to express the pitch of her voice, she writes in uppercase every word that has an even length (punctuation doesn't count).
To make her happy, you must help her by programming a helper for the keyboard.

---

## Input

The text that she wants to send, 1 line

---

## Output

The text edited by the keyboard helper, 1 line.

---

## Constraints

0 < text length < 512
Punctuation is limited to . ' ! ?.

---

## Example

#### Input

```
Are you stupid?
```

#### Output

```
Are you STUPID?
```
