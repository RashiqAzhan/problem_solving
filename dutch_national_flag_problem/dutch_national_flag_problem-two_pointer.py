def sort(array: [int]) -> [int]:
    start_index = 0
    end_index = len(array) - 1
    i = 0
    while i <= end_index:
        current_value = array[i]
        if current_value == 0:
            array = swap(array, i, start_index)
            start_index += 1
            i += 1
        elif current_value == 1:
            i += 1
        elif current_value == 2:
            array = swap(array, i, end_index)
            end_index -= 1

    return array


def swap(array: [int], a_index: int, b_index: int) -> [int]:
    temp = array[a_index]
    array[a_index] = array[b_index]
    array[b_index] = temp
    return array


def main():
    array = [2, 2, 0, 1, 2, 0]
    result = sort(array)
    print(result)


main()
