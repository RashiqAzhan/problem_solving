def smallest_subarray_with_a_given_sum(
        array: list[int],
        subarray_sum_limit: int):
    current_subarray_len = 0
    min_subarray_len = len(array)
    current_subarray_sum = 0
    i = 0
    while i < len(array):
        if current_subarray_sum < subarray_sum_limit:
            current_subarray_len += 1
            current_subarray_sum += array[i]
            i += 1
        else:
            min_subarray_len = min(current_subarray_len, min_subarray_len)
            subarray_starting_index = i - current_subarray_len
            current_subarray_sum -= array[subarray_starting_index]
            current_subarray_len -= 1

    return min_subarray_len


def main():
    array = [2, 1, 5, 2, 8]
    sum_limit = 7
    result = smallest_subarray_with_a_given_sum(array, sum_limit)
    print(result)


main()
