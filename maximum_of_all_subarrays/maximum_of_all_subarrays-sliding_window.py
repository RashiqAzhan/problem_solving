def max_sum_of_contiguous_subarray(array: list[int], subarray_length: int):
    first_subarray_sum = sum(array[:subarray_length])
    max_sum = first_subarray_sum
    previous_subarray_sum = first_subarray_sum

    for i in range(subarray_length, len(array)):
        first_subarray_element = array[i - subarray_length]
        next_array_element = array[i]
        current_subarray_sum = previous_subarray_sum - \
            first_subarray_element + next_array_element
        max_sum = max(current_subarray_sum, max_sum)
        previous_subarray_sum = current_subarray_sum

    return max_sum


def main():
    array = [2, 3, 4, 1, 5]
    k = 2
    result = max_sum_of_contiguous_subarray(array, k)
    print(result)


main()
