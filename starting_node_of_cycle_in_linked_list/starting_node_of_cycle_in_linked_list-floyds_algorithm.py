class Node:
    def __init__(self, value, next=None):
        self.value = value
        self.next = next

    def print_list(self):
        temp = self
        while temp is not None:
            print(f"{temp.value }")
            temp = temp.next
        print()


def find_cycle_start(head: Node):
    slow_pointer = head
    fast_pointer = head
    cycle_length = 0

    while fast_pointer.next.next is not None:
        fast_pointer = fast_pointer.next.next
        slow_pointer = slow_pointer.next
        if slow_pointer == fast_pointer:
            cycle_length = find_cycle_length(slow_pointer)
            break

    return find_start(head, cycle_length)


def find_cycle_length(slow_pointer: Node):
    current_pointer = slow_pointer.next
    cycle_length = 1
    while slow_pointer != current_pointer:
        current_pointer = current_pointer.next
        cycle_length += 1

    return cycle_length


def find_start(head: Node, cycle_length: int):
    first_pointer = head
    second_pointer = head

    for _ in range(cycle_length):
        second_pointer = second_pointer.next

    while first_pointer != second_pointer:
        first_pointer = first_pointer.next
        second_pointer = second_pointer.next

    return first_pointer


def main():
    head = Node(1)
    head.next = Node(2)
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)
    head.next.next.next.next.next = Node(6)

    head.next.next.next.next.next.next = head.next.next
    print(f"LinkedList has cycle: {find_cycle_start(head).value}")

    head.next.next.next.next.next.next = head.next.next.next
    print(f"LinkedList has cycle: {find_cycle_start(head).value}")

    head.next.next.next.next.next.next = head
    print(f"LinkedList has cycle: {find_cycle_start(head).value}")


main()
