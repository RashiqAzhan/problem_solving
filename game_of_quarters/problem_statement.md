## Goal

Alice and Bob are playing a game of quarters. Each are going to try to predict a sequence of 3 coin flips that will appear before the other players sequence of 3 coin flips appear. They then toss a coin 20 times recording either H for heads and T for tails. They then reveal their predictions and determine who won the round.

Example:
Alice predicts HTH
Bob predicts TTH
The coin flips: HTTHHTHTTHHHTHTHHTHH
Bob wins since TTH appears before HTH

If neither Alice's nor Bob's sequence appears in the round then neither person wins.
If Alice and Bob have the same sequence and it appears in the round then both people win.

They play multiple rounds of this game and the person with the most rounds won is the overall winner.

---

## Input

Line 1: An integer `N` to indicate the number of rounds.
`N` Lines Alice's 3 sequence prediction followed by a space followed by Bob's 3 sequence prediction followed by a space followed by the recorded 20 tosses of a coin

---

## Output

If Alice wins more rounds than Bob then Alice wins! should be printed.
If Bob wins more rounds than Alice then Bob wins! should be printed.
If neither has more wins than each other then Draw! should be printed.

---

## Constraints

1 <= `N` <= 20

---

## Example

#### Input

```
1
HTH TTH HTTHHTHTTHHHTHTHHTHH
```

#### Output

```
Bob wins!
```
