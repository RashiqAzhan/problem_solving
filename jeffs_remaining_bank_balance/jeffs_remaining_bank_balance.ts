const testCase1Input1 = `40 3`;
const testCase1Input2 = `W 120
D 120
D 30`;

const initialBalance = parseInt(testCase1Input1.replace(/\s.*/, ""));

const finalBalance =
  testCase1Input2
    .split(/r|\r?\n/)
    .map((line) =>
      line
        .split(/\s/)
        .map((word) =>
          isNaN(parseInt(word)) ? word : parseInt(word),
        ) as [string, number],
    )
    .reduce((balance, [transaction, amount]) =>
        balance + (transaction === "W" ? -amount : amount)
      , initialBalance);

console.log(finalBalance);
