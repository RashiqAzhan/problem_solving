def longest_substring_after_replacement(
        string: str, number_of_replacements: int):
    max_character_count = 0
    character_occurrences = {}
    window_start = 0
    character_with_highest_frequency = 0
    for window_end, character in enumerate(string):
        if character not in character_occurrences:
            character_occurrences[character] = 0
        character_occurrences[character] += 1
        character_with_highest_frequency = max(
            character_with_highest_frequency,
            character_occurrences[character])

        while window_end - window_start + 1 - \
                character_with_highest_frequency > number_of_replacements:
            first_character_in_window = string[window_start]
            if character_occurrences[first_character_in_window] == 1:
                del character_occurrences[first_character_in_window]
            else:
                character_occurrences[first_character_in_window] -= 1
            window_start += 1

        max_character_count = max(
            max_character_count,
            window_end - window_start + 1)

    return max_character_count


def main():
    string = "abccde"
    number_of_replacements = 1
    result = longest_substring_after_replacement(
        string, number_of_replacements)
    print(result)


main()
