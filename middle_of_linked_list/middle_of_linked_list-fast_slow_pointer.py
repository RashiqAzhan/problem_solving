class Node:

    def __init__(self, value, next=None):
        self.value = value
        self.next = next


def find_middle_of_linked_list(head: Node) -> Node:
    slow_pointer = head
    fast_pointer = head
    item_count = 1
    while fast_pointer.next:
        fast_pointer = fast_pointer.next
        item_count += 1
        if item_count % 2 == 0:
            slow_pointer = slow_pointer.next
    return slow_pointer


def main():
    head = Node(1)
    head.next = Node(2)
    head.next.next = Node(3)
    head.next.next.next = Node(4)
    head.next.next.next.next = Node(5)

    print("Middle Node: " + str(find_middle_of_linked_list(head).value))

    head.next.next.next.next.next = Node(6)
    print("Middle Node: " + str(find_middle_of_linked_list(head).value))

    head.next.next.next.next.next.next = Node(7)
    print("Middle Node: " + str(find_middle_of_linked_list(head).value))


main()
