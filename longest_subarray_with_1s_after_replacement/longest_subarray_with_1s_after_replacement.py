def longest_subarray_with_1s_after_replacement(
        array: list[int], number_of_replacements: int):
    window_start = 0
    max_subarray_length = 0
    zero_frequency = {0: 0}

    for window_end, number in enumerate(array):
        if number == 0:
            zero_frequency[number] += 1

        while zero_frequency[0] > number_of_replacements:
            window_starting_element = array[window_start]
            if window_starting_element in zero_frequency:
                zero_frequency[window_starting_element] -= 1
            window_start += 1

        max_subarray_length = max(
            max_subarray_length,
            window_end - window_start + 1)

    return max_subarray_length


def main():
    array = [0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1]
    number_of_replacements = 2
    result = longest_subarray_with_1s_after_replacement(
        array, number_of_replacements)
    print(result)


main()
