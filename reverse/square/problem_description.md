## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
101
```

#### Output

```
10201
```

---

#### Input

```
1111
```

#### Output

```
1234321
```

---

#### Input

```
264
```

#### Output

```
69696
```

---

#### Input

```
22
```

#### Output

```
484
```
