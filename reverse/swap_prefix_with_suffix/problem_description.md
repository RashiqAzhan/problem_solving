## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
5
breakfast
```

#### Output

```
fastbreak
```

---

#### Input

```
2
ha
```

#### Output

```
ha
```

---

#### Input

```
5
WorldHello
```

#### Output

```
HelloWorld
```

---

#### Input

```
4
rainT
```

#### Output

```
Train
```

---

#### Input

```
9
mind
```

#### Output

```
indm
```
