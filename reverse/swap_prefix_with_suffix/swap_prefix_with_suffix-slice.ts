const testCase1 = `5
breakfast`;

const testCase2 = `2
ha`;

const testCase3 = `5
WorldHello`;

const testCase4 = `4
rainT`;

const testCase5 = `9
mind`;

const [prefixLen, word] = testCase1.split(/\r|\r?\n/);
const prefixLenNum = parseInt(prefixLen) % word.length;

console.log(word.slice(prefixLenNum).concat(word.slice(0, prefixLenNum)));
