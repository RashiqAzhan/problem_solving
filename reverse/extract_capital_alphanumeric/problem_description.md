## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
.2A1N5Y64! §C*H*zAtrR
```

#### Output

```
ANYCHAR
```

---

#### Input

```
ahHdnDbiU
```

#### Output

```
HDU
```

---

#### Input

```
Ab
```

#### Output

```
A
```

---

#### Input

```
Hello World
```

#### Output

```
HW
```

---

#### Input

```
Homer J. Simpson
```

#### Output

```
HJS
```

---

#### Input

```
#1Cb2dA3h6P?9qS8
```

#### Output

```
CAPS
```
