const testCase1 = `.2A1N5Y64! §C*H*zAtrR`;
const testCase2 = `ahHdnDbiU`;
const testCase3 = `Ab`;
const testCase4 = `Hello World`;
const testCase5 = `Homer J. Simpson`;
const testCase6 = `#1Cb2dA3h6P?9qS8`;

console.log(
  testCase1
    .split("")
    .reduce((extract, item) =>
        item.charCodeAt(0) >= "A".charCodeAt(0)
        && item.charCodeAt(0) <= "Z".charCodeAt(0)
        ? extract.concat(item)
        : extract
      , ""),
);
