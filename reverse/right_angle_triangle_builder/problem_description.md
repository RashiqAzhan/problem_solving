## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
5
3
# & O *
```

#### Output

```
#    #    #
#&   #&   #&
#*&  #*&  #*&
#**& #**& #**&
#OOO&#OOO&#OOO&
```

---

#### Input

```
30
1
A B C D
```

#### Output

```
A
AB
ADB
ADDB
ADDDB
ADDDDB
ADDDDDB
ADDDDDDB
ADDDDDDDB
ADDDDDDDDB
ADDDDDDDDDB
ADDDDDDDDDDB
ADDDDDDDDDDDB
ADDDDDDDDDDDDB
ADDDDDDDDDDDDDB
ADDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDDDDDB
ADDDDDDDDDDDDDDDDDDDDDDDDDDDB
ACCCCCCCCCCCCCCCCCCCCCCCCCCCCB
```

---

#### Input

```
1
5
# * % O
```

#### Output

```
#####
```

---

#### Input

```
10
3
* * * *
```

#### Output

```
*         *         *
**        **        **
***       ***       ***
****      ****      ****
*****     *****     *****
******    ******    ******
*******   *******   *******
********  ********  ********
********* ********* *********
******************************
```

---

#### Input

```
15
3
# @ @ +
```

#### Output

```
#              #              #
#@             #@             #@
#+@            #+@            #+@
#++@           #++@           #++@
#+++@          #+++@          #+++@
#++++@         #++++@         #++++@
#+++++@        #+++++@        #+++++@
#++++++@       #++++++@       #++++++@
#+++++++@      #+++++++@      #+++++++@
#++++++++@     #++++++++@     #++++++++@
#+++++++++@    #+++++++++@    #+++++++++@
#++++++++++@   #++++++++++@   #++++++++++@
#+++++++++++@  #+++++++++++@  #+++++++++++@
#++++++++++++@ #++++++++++++@ #++++++++++++@
#@@@@@@@@@@@@@@#@@@@@@@@@@@@@@#@@@@@@@@@@@@@@
```
