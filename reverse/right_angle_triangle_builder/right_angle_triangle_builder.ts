const testCase1 = `5
3
# & O *`;

const testCase2 = `30
1
A B C D`;

const testCase3 = `1
5
# * % O`;

const testCase4 = `10
3
* * * *`;

const testCase5 = `15
3
# @ @ +`;

const triangleBuilder = (width: number, leftSymbol: string, rightSymbol: string, bottomSymbol: string, fillSymbol: string): string =>
  Array.from({length: width},
    (_, i) =>
      `${leftSymbol}`
      + `${fillSymbol.repeat(i > 0 && i < width - 1 ? i - 1 : 0)}`
      + `${bottomSymbol.repeat(i > 0 && i === width - 1 ? width - 2 : 0)}`
      + `${rightSymbol.repeat(i > 0 ? 1 : 0)}`,
  ).join("\n");

const [width, numberOfTriangles, [leftSymbol, rightSymbol, bottomSymbol, fillSymbol]] =
  testCase1
    .split(/\r|\r?\n/)
    .map((line) => Number(line) ? Number(line) : line.split(" ")) as [number, number, string[]];

console.log(
  triangleBuilder(width, leftSymbol, rightSymbol, bottomSymbol, fillSymbol)
    .split(/\r|\r?\n/)
    .map((line) =>
      `${line}${width > 1 ? " ".repeat(Math.abs(width - line.length)) : ""}`
        .repeat(numberOfTriangles)
        .trimEnd())
    .join("\n"),
);
