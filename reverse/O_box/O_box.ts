const testCase1 = `2
2`;

const testCase2 = `4
1`;

const testCase3 = `20
20`;

const testCase4 = `1
1`;

const makeBox = (rows: number, columns: number, char: string = "O"): string =>
    Array(rows)
        .fill(null)
        .reduce((box, _) => "".concat(box, char.repeat(columns)), "");

console.log(makeBox(...testCase1.split(/\r?\n|\r/g)));
