const testCase1 = 0;
const testCase2 = 1;
const testCase3 = 2;
const testCase4 = 3;
const testCase5 = 4;
const testCase6 = 5;
const testCase7 = 6;
const testCase8 = 7;

const fibonacciTailRecursion = (n: number, a = 0, b = 1): number => {
  if (n === 0) {
    return 0;
  }

  return n === 1
    ? b
    : fibonacciTailRecursion(n - 1, b, a + b);
}

console.log(fibonacciTailRecursion(testCase1));
