## Goal

The game mode is REVERSE: You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
0
```

#### Output

```
0
```

---

#### Input

```
1
```

#### Output

```
1
```

---

#### Input

```
2
```

#### Output

```
1
```

---

#### Input

```
3
```

#### Output

```
2
```

---

#### Input

```
4
```

#### Output

```
3
```

---

#### Input

```
5
```

#### Output

```
5
```

---

#### Input

```
6
```

#### Output

```
8
```

---

#### Input

```
7
```

#### Output

```
13
```

---

#### Input

```
8
```

#### Output

```
21
```
