const testCase1 = 0;
const testCase2 = 1;
const testCase3 = 2;
const testCase4 = 3;
const testCase5 = 4;
const testCase6 = 5;
const testCase7 = 6;
const testCase8 = 7;

const fibonacci = (n: number): number => {
  const phi = (1 + Math.sqrt(5)) / 2;

  return Math.round((Math.pow(phi, n) - Math.pow(1 - phi, n)) / (2 * phi - 1));
};

console.log(fibonacci(testCase1));
