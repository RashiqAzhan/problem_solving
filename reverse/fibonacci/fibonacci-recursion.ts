const testCase1 = 0;
const testCase2 = 1;
const testCase3 = 2;
const testCase4 = 3;
const testCase5 = 4;
const testCase6 = 5;
const testCase7 = 6;
const testCase8 = 7;

const fibonacci = (n: number): number =>
  n <= 1
  ? n
  : fibonacci(n - 1) + fibonacci(n - 2);

console.log(fibonacci(testCase1));
