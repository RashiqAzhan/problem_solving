const testCase1 = `A1!`;
const testCase2 = `abc123///`;
const testCase3 = `This is an integer: 1,000,000`;
const testCase4 = `White-spaces count as symbols`;
const testCase5 = `1z+4z+2z=7z`;
const testCase6 = `Concatenate #Strings, #Numbers and #Symbols to get 40511`;
const testCase7 = `RAGE QUIT!!!`;

console.log(
  [...testCase1]
    .reduce((acc, char) => {
      char.toLocaleUpperCase().charCodeAt(0) >= 65 && char.toLocaleUpperCase().charCodeAt(0) <= 90
      ? ++acc[0]
      : Number(char)
        ? ++acc[1]
        : ++acc[2];

      return acc;
    }, [0, 0, 0])
    .join(""),
);
