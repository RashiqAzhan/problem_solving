## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
A1!
```

#### Output

```
111
```

---

## Example

#### Input

```
abc123///
```

#### Output

```
333
```

---

#### Input

```
This is an integer: 1,000,000
```

#### Output

```
1577
```

---

#### Input

```
White-spaces count as symbols
```

#### Output

```
2504
```

---

#### Input

```
1z+4z+2z=7z
```

#### Output

```
443
```

---

#### Input

```
Concatenate #Strings, #Numbers and #Symbols to get 40511
```

#### Output

```
40511
```

---

#### Input

```
RAGE QUIT!!!
```

#### Output

```
804
```
