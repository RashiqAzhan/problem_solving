const testCase1 = `A1!`;
const testCase2 = `abc123///`;
const testCase3 = `This is an integer: 1,000,000`;
const testCase4 = `White-spaces count as symbols`;
const testCase5 = `1z+4z+2z=7z`;
const testCase6 = `Concatenate #Strings, #Numbers and #Symbols to get 40511`;
const testCase7 = `RAGE QUIT!!!`;

console.log(
  `${testCase1.match(/([a-zA-Z])/g)?.length ?? "0"}`
  + `${testCase1.match(/(\d)/g)?.length ?? "0"}`
  + `${testCase1.match(/([\s\W])/g)?.length ?? "0"}`,
);
