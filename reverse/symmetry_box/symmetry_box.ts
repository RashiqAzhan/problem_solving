const testCase1 = `#O
3 3`;

const testCase2 = `#O
5 5`;

const testCase3 = `#O
1 1`;

const testCase4 = `#O
5 3`;

const testCase5 = `#O
3 5`;

const testCase6 = `#O
6 6`;

const testCase7 = `ABCDEF
11 11`;

const testCase8 = `#@%<>O
20 13`;

const [[symbols], [width, height]] =
  testCase1
    .split(/\r|\r?\n/)
    .map((line) => line.split(" "));

const makeBox = (symbols: string, width: number, height: number): string =>
  [...Array(height)].map((_, colIndex) =>
    [...Array(width)].map(() =>
      "").reduce((row, _, rowIndex) =>
        rowIndex >= width / 2
        ? row.concat(row[width - rowIndex - 1])
        : row.concat(symbols[Math.min(rowIndex, colIndex)] ?? symbols[0])
      , ""),
  ).reduce((box, row, colIndex) =>
      colIndex >= height / 2
      ? [...box, box[height - colIndex - 1]]
      : [...box, row]
    , [] as string[]).join("\n");

console.log(makeBox(symbols, +width, +height));
