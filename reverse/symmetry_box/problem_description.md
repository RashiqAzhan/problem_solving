## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
#O
3 3
```

#### Output

```
###
#O#
###
```

---

#### Input

```
#O
5 5
```

#### Output

```
#####
#OOO#
#O#O#
#OOO#
#####
```

---

#### Input

```
#O
1 1
```

#### Output

```
#
```

---

#### Input

```
#O
5 3
```

#### Output

```
#####
#OOO#
#####
```

---

#### Input

```
#O
3 5
```

#### Output

```
###
#O#
#O#
#O#
###
```

---

#### Input

```
#O
6 6
```

#### Output

```
######
#OOOO#
#O##O#
#O##O#
#OOOO#
######
```

---

#### Input

```
ABCDEF
11 11
```

#### Output

```

AAAAAAAAAAA
ABBBBBBBBBA
ABCCCCCCCBA
ABCDDDDDCBA
ABCDEEEDCBA
ABCDEFEDCBA
ABCDEEEDCBA
ABCDDDDDCBA
ABCCCCCCCBA
ABBBBBBBBBA
AAAAAAAAAAA
```

---

#### Input

```
#@%<>O
20 13
```

#### Output

```
####################
#@@@@@@@@@@@@@@@@@@#
#@%%%%%%%%%%%%%%%%@#
#@%<<<<<<<<<<<<<<%@#
#@%<>>>>>>>>>>>><%@#
#@%<>OOOOOOOOOO><%@#
#@%<>O########O><%@#
#@%<>OOOOOOOOOO><%@#
#@%<>>>>>>>>>>>><%@#
#@%<<<<<<<<<<<<<<%@#
#@%%%%%%%%%%%%%%%%@#
#@@@@@@@@@@@@@@@@@@#
####################
```
