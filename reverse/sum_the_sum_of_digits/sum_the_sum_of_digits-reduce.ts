const testCase1 = 54;

console.log(testCase1
  + testCase1
    .toString()
    .split("")
    .map(Number)
    .reduce((a, b) =>
        a + b
      , 0),
);
