## Goal

You do not have access to the statement. You have to guess what to do by observing the following set of tests:

---

## Example

#### Input

```
5
```

#### Output

```
*
 **
  ***
   ****
    *****
```

---

#### Input

```
4
```

#### Output

```
*
 **
  ***
   ****
```

---

#### Input

```
1
```

#### Output

```
*
```

---

#### Input

```
2
```

#### Output

```
*
 **
```
