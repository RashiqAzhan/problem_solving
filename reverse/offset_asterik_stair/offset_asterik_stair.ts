const testCase1 = 5;
const testCase2 = 4;
const testCase3 = 1;
const testCase4 = 2;

console.log(
  Array
    .from({length: testCase1}, (_, i) =>
      " ".repeat(i) + "*".repeat(++i))
    .join("\n"),
);
