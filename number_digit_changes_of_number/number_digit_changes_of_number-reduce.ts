const testCase1 = `123`;

console.log(
  testCase1
    .split("")
    .reduce((count, value, index, digits) =>
        value !== digits[++index] ? ++count : count
      , -1),
);
