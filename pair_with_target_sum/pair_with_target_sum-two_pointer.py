def pair_with_sum(array: list[int], target_sum: int):
    starting_pointer = 0
    ending_pointer = len(array) - 1
    pair_indexes = (-1, -1)

    while (starting_pointer < ending_pointer):
        current_sum = array[starting_pointer] + array[ending_pointer]
        if current_sum < target_sum:
            starting_pointer += 1
        elif current_sum > target_sum:
            ending_pointer -= 1
        else:
            pair_indexes = (starting_pointer, ending_pointer)
            break

    return pair_indexes


def main():
    array = [1, 2, 3, 4, 6]
    target_sum = 6
    result = pair_with_sum(array, target_sum)
    print(result)


main()
