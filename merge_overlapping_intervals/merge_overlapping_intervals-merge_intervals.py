class Interval:

    def __init__(self, start, end):
        self.start = start
        self.end = end

    def print_interval(self):
        print(f"[{str(self.start)}, {str(self.end)}]", end="")


def merge(intervals):
    merged = []
    sorted_intervals = sorted(intervals, key=lambda x: x.start)

    previous_interval = sorted_intervals[0]
    i = 0
    while i < len(sorted_intervals) + 1:
        first_interval = previous_interval
        if i < len(sorted_intervals) - 1:
            second_interval = sorted_intervals[i + 1]
        elif i == len(sorted_intervals):
            merged.append(previous_interval)

        if first_interval.start < second_interval.start < first_interval.end < second_interval.end:
            previous_interval = Interval(first_interval.start, second_interval.end)
            i += 1
        elif first_interval.start < second_interval.start < second_interval.end < first_interval.end:
            previous_interval = Interval(first_interval.start, first_interval.end)
            i += 1
        else:
            merged.append(previous_interval)
            previous_interval = second_interval
            if merged[-1] == previous_interval:
                break

    return merged


def main():
    print("Merge intervals: ", end="")
    for i in merge([Interval(1, 4), Interval(2, 5), Interval(7, 9)]):
        i.print_interval()
    print()

    print("Merge intervals: ", end="")
    for i in merge([Interval(6, 7), Interval(2, 4), Interval(5, 9)]):
        i.print_interval()
    print()

    print("Merge intervals: ", end="")
    for i in merge([Interval(1, 4), Interval(2, 6), Interval(3, 5)]):
        i.print_interval()
    print()


main()
