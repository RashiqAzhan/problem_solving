def is_happy_number(number: int) -> bool:
    fast_pointer = number
    slow_pointer = number

    while True:
        slow_pointer = digit_squared_sum(slow_pointer)
        fast_pointer = digit_squared_sum(digit_squared_sum(fast_pointer))
        if fast_pointer == slow_pointer:
            break

    if fast_pointer == 1:
        return True
    else:
        return False


def digit_squared_sum(number: int) -> int:
    sum = 0
    for digit in str(number):
        digit = int(digit)
        sum += digit ** 2

    return sum


def main():
    number = 23
    result = is_happy_number(number)
    print(result)


main()
