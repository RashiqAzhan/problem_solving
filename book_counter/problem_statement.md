## Goal

In the following test cases you must find out how many books are in each section of the library. You must find out using the section number using the first one or two characters of it as follows:

SF = science fiction
J = junior
M = mystery
R = Romance
F = fantasy

---

## Input

Line 1: Book category that is to be counted
Line 2: An integer, `N`, representing the amount of books in the case
Next `N` lines: Different strings representing different types of books with categories

---

## Output

Line 1: The number of books in a category

---

## Constraints

1 <= `N` <= 20

---

## Example

#### Input

```
J
2
SF10384
J28374
```

#### Output

```
1
```
