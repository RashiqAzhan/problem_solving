const testCase1 = `J
2
SF10384
J28374`;

const [bookCategory, bookCount, ...books] = testCase1.split(/\r|\r?\n/);

console.log(
  books
    .reduce((count, book) =>
        book.replace(/\d/g, "") === bookCategory ? ++count : count
      , 0),
);
