const testCase1 = `3
images/beachTrip.jpg
newFolder/oakTree.png
images/highShool.eps`;

const [fileCount, ...fileNames] =
  testCase1
    .split(/\r|\r?\n/)
    .map((line) => Number(line) ? Number(line) : line) as [number, string];

console.log(
  fileNames.map((filePath) =>
    (filePath.endsWith(".jpg") || filePath.endsWith(".png")) ?

    filePath.slice(0, filePath.indexOf("/") + 1)
      + filePath
        .slice(filePath.indexOf("/") + 1, filePath.indexOf("."))
        .split("")
        .map((char) => /[A-Z]/.test(char) ? `_${char.toLowerCase()}` : char)
        .join("")
      + filePath.slice(filePath.indexOf(".")) : filePath,
  ).join("\n"),
);
