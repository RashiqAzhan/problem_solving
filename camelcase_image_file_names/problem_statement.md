## Goal

Given a list of files, you have to change the file name of only .jpg and .png files from the camelCase style to snake_case style. Files with any other extension should not be changed.

---

## Input

Line 1: An integer `N` for the number of lines you should read below.
Next `N` lines: `F` relative path + file name.

---

## Output

`N` lines: `F` relative path + file name with the requested changes, in the same order as the input.

---

## Constraints

3 ≤ `N` ≤ 7
camelCase file names are guaranteed to start with a lowercase letter.

---

## Example

#### Input

```
3
images/beachTrip.jpg
newFolder/oakTree.png
images/highShool.eps
```

#### Output

```
images/beach_trip.jpg
newFolder/oak_tree.png
images/highShool.eps
```
