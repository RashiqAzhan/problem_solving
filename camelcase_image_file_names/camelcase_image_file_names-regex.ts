const testCase1 = `3
images/beachTrip.jpg
newFolder/oakTree.png
newFolder/theDarkMoon.png
images/highShool.eps`;

const [fileCount, ...fileNames] =
  testCase1
    .split(/\r|\r?\n/)
    .map((line) => Number(line) ? Number(line) : line) as [number, string];

console.log(
  fileNames.map((filePath) => {
      [...filePath.matchAll(/(?<=\/.*)[A-Z](?=.*\.(?:jpg|png))/g)].reverse().forEach((match) => {
        filePath = filePath.replace(new RegExp(`(?<=.{${match.index}}).`), `_${match[0].toLowerCase()}`);
      });

      return filePath;
    },
  ).join("\n"),
);
