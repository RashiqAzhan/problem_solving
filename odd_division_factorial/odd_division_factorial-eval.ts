const testCase1 = 5;
const testCase2 = 8;

const expression =
  Array.from({length: testCase1}, (_, i) => ++i)
       .reverse()
       .flatMap((number, index) => [index !== 0 && number % 2 !== 0 ? "/" : "*", number])
       .slice(1)
       .join("");

console.log(Math.round(eval(expression)));
