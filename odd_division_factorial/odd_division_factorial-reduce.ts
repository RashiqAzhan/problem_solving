const testCase1 = 5;
const testCase2 = 8;

const result =
  Array.from({length: testCase1 - 1}, (_, i) => ++i)
       .reverse()
       .reduce((acc, number) =>
           number % 2 === 0 ? acc * number : acc / number
         , testCase1);

console.log(~~(result + 0.5));
