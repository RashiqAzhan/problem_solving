def find_triplet_count(array: list[int], max_sum_limit: int):
    array = sorted(array)
    triplet_count = 0
    for i in range(len(array) - 2):
        max_pair_sum_limit = max_sum_limit - array[i]
        pair_list = find_pair(array, max_pair_sum_limit, left_pointer = i+1, right_pointer = len(array) - 1)
        print(pair_list)
        triplet_count += len(pair_list)

    return triplet_count


def find_pair(array: list[int], max_pair_sum_limit: int, left_pointer: int, right_pointer: int):
    pair_list = []
    while left_pointer < right_pointer:
        left_pointer_value = array[left_pointer]
        right_pointer_value = array[right_pointer]
        pair_sum = left_pointer_value + right_pointer_value
        if pair_sum >= max_pair_sum_limit:
            right_pointer -= 1
            continue
        while left_pointer < right_pointer:
            pair = [left_pointer_value, right_pointer_value]
            pair_list.append(pair)
            right_pointer -= 1
            right_pointer_value = array[right_pointer]

        left_pointer += 1
        right_pointer = len(array) - 1

    return pair_list


def main():
    array = [-1, 4, 2, 1, 3]
    max_sum_limit = 5 # excluding
    result = find_triplet_count(array, max_sum_limit)
    print(result)


main()
