def merge(first_intervals: [int], second_intervals: [int]) -> [int]:
    result = []
    first_intervals_index = 0
    second_intervals_index = 0

    while first_intervals_index < len(first_intervals) and second_intervals_index < len(second_intervals):
        first_intervals_entry = first_intervals[first_intervals_index]
        second_intervals_entry = second_intervals[second_intervals_index]

        if first_intervals_entry[1] < second_intervals_entry[0]:
            first_intervals_index += 1
            first_intervals_entry = first_intervals[first_intervals_index]
        elif second_intervals_entry[1] < first_intervals_entry[0]:
            second_intervals_index += 1
            second_intervals_entry = second_intervals[second_intervals_index]

        merged_interval = [max(first_intervals_entry[0], second_intervals_entry[0]),
                           min(first_intervals_entry[1], second_intervals_entry[1])]
        result.append(merged_interval)

        if merged_interval[1] == first_intervals_entry[1]:
            first_intervals_index += 1
        elif merged_interval[1] == second_intervals_entry[1]:
            second_intervals_index += 1

    return result


def main():
    print(f"Intervals Intersection: {merge([[1, 3], [5, 6], [7, 9]], [[2, 3], [5, 7]])}")
    print(f"Intervals Intersection: {merge([[1, 3], [4, 7], [9, 12]], [[5, 10]])}")


main()
