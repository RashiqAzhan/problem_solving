const arr1 = [3, 1, 0, 5, 3];
const arr2 = [2, 1, 8, 9, 2];
const targetSum = 5;

const result: [number, number][] = [];

for (let i = 0; i < arr1.length; ++i) {
  for (let j = 0; j < arr2.length; ++j) {
    if (arr1[i] + arr2[j] === targetSum) {
      result.push([i, j]);
    }
  }
}

console.log(result.join("\n"));
