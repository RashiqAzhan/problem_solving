const arr1 = [3, 1, 0, 5, 3];
const arr2 = [2, 1, 8, 9, 2];
const targetSum = 5;

const arr2Map =
  arr2.reduce<Record<string, number[]>>((map, value, index) =>
      ({...map, [value]: [...map[value] || [], index]})
    , {});

console.log(arr1
  .map((value) => Math.abs(targetSum - value))
  .reduce((pairIndices, delta, index) => [
    ...pairIndices,
    ...arr2Map[delta]?.map<[number, number]>((arr2Index) => [index, arr2Index]) || [],
  ], [] as [number, number][])
  .join("\n"),
);
