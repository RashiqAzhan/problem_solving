const arr1 = [3, 1, 0, 5, 3];
const arr2 = [2, 1, 8, 9, 2];
const targetSum = 5;

const arr2Map =
  arr2.reduce<Record<string, number[]>>((map, value, index) =>
      ({...map, [value]: [...map[value] || [], index]})
    , {});

const result: [number, number][] = [];
for (let i = 0; i < arr1.length; ++i) {
  const delta = Math.abs(targetSum - arr1[i]);
  if (arr2Map[delta]) {
    result.push(...arr2Map[delta].map<[number, number]>((arr2Index) => [i, arr2Index]));
  }
}

console.log(result.join("\n"));
