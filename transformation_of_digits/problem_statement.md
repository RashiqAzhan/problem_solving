## Goal

We define a transformation of digits as being from "0123456789" to "9876543210".
You're given `N` digits as input. Please transform the digits in a similar way.
For example, if you're given the digit 1, you should output the digit 8.

---

## Input

Line 1: An integer `N` for the number of digits to input.
Next `N` lines: `X` as the digit.

---

## Output

Next `N` lines: The transformation of `X`

---

## Constraints

0 ≤ `X` ≤ 9

---

## Example

#### Input

```
1
8
```

#### Output

```
1
```
