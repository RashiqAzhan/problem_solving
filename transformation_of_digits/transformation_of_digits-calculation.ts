const testCase1 = `1
8`;
const testCase1Expected = `1`;

const testCase2 = `5
1
4
3
3
1`;
const testCase2Expected = `8
5
6
6
8`;

const testCase3 = `3
2
6
8`;
const testCase3Expected = `7
3
1`;

const testCase4 = `4
1
3
5
0`;
const testCase4Expected = `8
6
4
9`;

const [digitCount, ...digits] = testCase1.split(/\r?\r?\n/).map(Number);

digits.map((digit) => 9 - digit)
      .forEach((digit) => console.log(digit));
