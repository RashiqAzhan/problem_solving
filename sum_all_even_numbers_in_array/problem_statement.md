## Goal

Write a function that takes an array of numbers and returns the sum of the even numbers in the array.

---

## Input

Line 1: An integer `n` - count of elements in an array.
Line 2: `n` space-separated integer numbers `x_1` ... `x_n`.

---

## Output

Sum of even numbers.

---

## Constraints

-100 <= `x_i` <= 100

---

## Example

#### Input

```
4
1 2 3 4
```

#### Output

```
6
```
