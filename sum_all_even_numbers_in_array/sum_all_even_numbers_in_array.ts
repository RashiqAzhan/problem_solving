const testCase1 = "1 2 3 4";

console.log(
  testCase1
    .split(" ")
    .map(Number)
    .filter((number) => number % 2 === 0)
    .reduce((a, b) => a + b),
);
