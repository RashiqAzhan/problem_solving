const testCase1Input1 = "A**-*3* white coupe";
const testCase1Input2 = `ERS-32S white coupe
KSO-21D black sedan
ASF-23K white coupe
AS-AS-32 black SUV`;

const testCase2Input1 = "**-26-**-** brown SUV";
const testCase2Input2 = `HRXQ-83 purple coupe
55-O3-1F blue coupe
ZV-26-H1-XL brown SUV
W8-U4-F2-P4-7 brown SUV
VV-K1-13-13-T grey SUV
61-26-WW-NT brown SUV
X5-9A-J0-8 pink SUV
D4VS-O0CN brown SUV`;

const testCase3Input1 = "K4-3*-75 orange sedan";
const testCase3Input2 = `A99G-A17M-7 pink sedan
PO1X-BQ white sedan
K4-3D-75 orange sedan
K4-3X-752 orange sedan
7B-O2-R7-NQ-W grey SUV
JN-UB-8J-7 grey sedan
AJS-N09-AH black coupe
3K4-36-75 orange sedan
M6-1M-3T-BK-T purple sedan
7VC-P3N purple sedan
WL-9W-1U-QU blue coupe
K4-32-75 orange sedan
402B-T40G-L yellow coupe
PO0-99Z blue sedan
Q71W-N0 red sedan
K4-37-75 orange sedan
NTA3-B2JW green coupe`;

const testCase4Input1 = "***-***-** purple sedan";
const testCase4Input2 = `NS6L-DN30-7 white SUV
X9YQ-LDSI-1 yellow SUV
717U-PPTV black sedan
IXEZ-NF purple sedan
16-5X-V2-60-I red coupe
D37-XG6-8T3 black coupe
84R-22Z-E51 purple sedan
O8TU-P63 purple SUV
0Y2-YEB-E blue SUV
U1C-UZK-I25 purple SUV
BU7-N3E-8 yellow SUV
20ZC-NP orange SUV
1J-AO-98-37-A blue SUV
WB-N7-E6-Y yellow coupe
JM0-N8D-L purple SUV
5SG-5I2-21 purple sedan
V17-TA6-SF purple sedan
FCN-O24-63A red coupe
U1IX-418H purple SUV
BY5-1B0 blue sedan
JN6-LGV-L8 white sedan
TW78-DSXQ-2 green SUV
3E-W6-VZ blue coupe
89K-3XH-4Y green coupe
6L7Q-8SNK-G orange coupe
01-93-52-9S white SUV
5FT-OP6-P red coupe
W4-7W-3E-L1 brown coupe
L83-A4I-GSA brown coupe
R4A-62W-ZR3 yellow coupe
6EEB-ZGQ9-I grey SUV
28-TC-JY brown SUV
1T-0Q-7F-13-S blue SUV
96G6-U8 blue SUV
44X-XW9-L58 blue coupe
2P-VX-84 orange coupe
EH-04-06-VC-9 orange coupe
JJ5-KWM-V2 orange coupe
04-JW-OQ grey sedan
8AN0-26LY pink sedan`;

const matchLicensePlate = (partialLicensePlate: string, suspectLicensePlate: string): boolean =>
  partialLicensePlate.length === suspectLicensePlate.length
  ? partialLicensePlate
    .split("")
    .map((char, index) => char === "*" ? suspectLicensePlate[index] : char)
    .join("") === suspectLicensePlate
  : false;

const findIndicesOfLicensePlateMatches = (partialLicensePlate: string, suspectLicensePlates: string[]) => {
  return suspectLicensePlates.reduce<number[]>((indices, suspectLicensePlate, index) => {
    return matchLicensePlate(partialLicensePlate, suspectLicensePlate) ? indices.concat(index) : indices;
  }, []);
};

const filterVehicleDescriptionMatches = (vehicleDescriptionGiven: string, suspectVehicleDescriptions: string[]) =>
  suspectVehicleDescriptions.filter((suspectVehicle) =>
    suspectVehicle.replace(/[^\s]*\s/, "") === vehicleDescriptionGiven.split(/\s(.*)/)[1],
  );


const recallVehicleDescription = testCase1Input1;
const suspectVehicleDescriptions = testCase1Input2;

const matchingMakeAndModelSuspectLicensePlates =
  filterVehicleDescriptionMatches(recallVehicleDescription, suspectVehicleDescriptions.split(/[\r|\r?\n]+/));
const partialLicensePlate = recallVehicleDescription.split(/\s/, 1).at(0) as string;
const indices = findIndicesOfLicensePlateMatches(partialLicensePlate,
  matchingMakeAndModelSuspectLicensePlates.map((suspectLicensePlates) => suspectLicensePlates.split(" ").at(0) as string),
);

console.log(
  indices
    .reduce<string[]>((licensePlateMatches, matchIndex) =>
        [...licensePlateMatches, matchingMakeAndModelSuspectLicensePlates[matchIndex]]
      , [])
    .join("\n"),
);
