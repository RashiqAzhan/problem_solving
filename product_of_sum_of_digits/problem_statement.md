## Goal

Given a string of space separated numbers, calculate the product of all the sums of the digits of each number.
e.g. given "3 32 12 50", the answer would be (3)*(3+2)*(1+2)*(5+0)=3*5*3*5=225, so you would output 225

---

## Input

Line 1: A string `x` of space separated numbers

---

## Output

One line, the product of the sums of each number's characters

---

## Constraints

1 <= The number of space separated numbers <= 10

---

## Example

#### Input

```
3 32 12 50
```

#### Output

```
225
```
