const testCase1 = "3 32 12 50";
const testCase2 = "333 22 5";

console.log(eval(
  `(${testCase1})`
    .replace(/(\d)(?=\d)/g, "$1+")
    .replace(/\s/g, ")*("),
));
