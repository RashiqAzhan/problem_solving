const testCase1 = "3 32 12 50";
const testCase2 = "333 22 5";

console.log(
  testCase1
    .split(" ")
    .reduce((product, number) =>
        +[...number].reduce<number>((a, b) => +b + a
          , 0)
        * product
      , 1),
);
