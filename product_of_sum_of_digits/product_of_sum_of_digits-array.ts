const testCase1 = "3 32 12 50";
const testCase2 = "333 22 5";

console.log(
  testCase1
    .split(" ")
    .map((number) => [...number].map(Number).reduce((a, b) => a + b))
    .reduce((a, b) => a * b),
);
