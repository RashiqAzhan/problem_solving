const testCase1 = "3 32 12 50";
const testCase2 = "333 22 5";

console.log(eval(
  testCase1
    .split(" ")
    .map((number) => `(${[...number].join("+")})`)
    .join("*"),
));
